import { Gender } from '@prisma/client';
import { Transform } from 'class-transformer';
import {
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { IsNotExist } from 'src/custom-validator/validators/is-not-exist.validator';

export class CreateDto {
  @IsNumber()
  @Transform(({ value }) => Number(value))
  @IsNotExist([{ model: 'user', where: { id: 'id' } }])
  id: number;

  @IsString()
  @MinLength(5)
  @MaxLength(20)
  @IsNotExist([{ model: 'user', where: { username: 'username' } }])
  username: string;

  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsEnum(Gender)
  gender: Gender;
}
