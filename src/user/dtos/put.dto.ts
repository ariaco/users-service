import { Gender } from '@prisma/client';
import { Transform } from 'class-transformer';
import {
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { IsExist } from 'src/custom-validator/validators/is-exist.validator';
import { IsUnique } from 'src/custom-validator/validators/is-unique.validator';

export class PutDto {
  @IsNumber()
  @Transform(({ value }) => Number(value))
  @IsExist([{ model: 'user', where: { id: 'id' } }])
  id: number;

  @IsString()
  @MinLength(5)
  @MaxLength(20)
  @IsUnique(['user', 'username', 'id'])
  username: string;

  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsEnum(Gender)
  gender: Gender;

  @IsNumber()
  @Transform(({ value }) => Number(value))
  amount: number;
}
