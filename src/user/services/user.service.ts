import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}
  async create({ data }: { data: Prisma.UserCreateInput }) {
    return this.prisma.user.create({ data });
  }
  async find({ where }: { where: Prisma.UserWhereInput }) {
    return this.prisma.user.findMany({ where });
  }
  async update({
    where,
    data,
  }: {
    where: Prisma.UserWhereUniqueInput;
    data: Prisma.UserUpdateInput;
  }) {
    return this.prisma.user.update({ where, data });
  }
}
