import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateDto } from '../dtos/create.dto';
import { UserService } from '../services/user.service';
import { ClientProxy } from '@nestjs/microservices';
import { GetDto } from '../dtos/get.dto';
import { WalletService } from 'src/wallet/services/wallet.service';
import { PutDto } from '../dtos/put.dto';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject('RABBITMQ_SERVICE') private client: ClientProxy,
    private readonly walletService: WalletService,
  ) {}

  @Post('')
  async create(@Body() body: CreateDto) {
    const user = await this.userService.create({ data: body });
    this.client.emit('user-created', { userId: user.id });
    return user;
  }

  @Get(':id')
  async get(@Param() param: GetDto) {
    const user = (await this.userService.find({ where: { id: param.id } }))[0];

    if (!user) return {};

    const amount = await this.walletService.get({ id: param.id });

    return {
      ...user,
      amount,
    };
  }

  @Put('')
  async update(@Body() body: PutDto) {
    const { id, amount, ...data }: any = body;

    return {
      ...(await this.userService.update({ where: { id: id }, data })),
      amount: await this.walletService.update({
        id: id,
        data: { amount },
      }),
    };
  }
}
