import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { CustomValidatorModule } from './custom-validator/custom-validator.module';
import { WalletModule } from './wallet/wallet.module';

@Module({
  imports: [UserModule, CustomValidatorModule, WalletModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
