import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
@ValidatorConstraint({ name: 'isExistValidator', async: true })
export class IsExistValidator implements ValidatorConstraintInterface {
  constructor(private readonly prisma: PrismaService) {}

  async validate(value: string | number, args: ValidationArguments) {
    const whereCondition: any = {};
    for (const constraint of args.constraints) {
      const { model, where } = constraint;
      for (const key in where) {
        whereCondition[key] = (args.object as any)[where[key]];
      }

      const data = await this.prisma[model].findMany({
        where: whereCondition,
      });

      if (data.length == 0) return false;
    }

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    return `No record exists.`;
  }
}

export function IsExist(
  values: Array<{ model: string; where: { [key: string]: string } }>,
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsExist',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: values,
      validator: IsExistValidator,
    });
  };
}
