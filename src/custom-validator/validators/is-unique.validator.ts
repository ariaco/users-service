import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
@ValidatorConstraint({ name: 'isUnique', async: true })
export class IsUniqueValidator implements ValidatorConstraintInterface {
  constructor(private readonly prisma: PrismaService) {}

  async validate(value: string | number, args: ValidationArguments) {
    const or = [];

    const [model, ...keys] = args.constraints;
    for (const key of keys) {
      if ((args.object as any)[key])
        or.push({ [key]: (args.object as any)[key] });
    }

    const data = await this.prisma[model].findMany({
      where: {
        OR: or,
      },
    });

    if (data && data.length >= 2) return false;

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    return `The ${args.constraints[1]} value must be unique.`;
  }
}

export function IsUnique(
  values: Array<string>,
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsUnique',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: values,
      validator: IsUniqueValidator,
    });
  };
}
