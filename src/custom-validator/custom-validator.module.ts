import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { IsExistValidator } from './validators/is-exist.validator';
import { IsNotExistValidator } from './validators/is-not-exist.validator';
import { IsUniqueValidator } from './validators/is-unique.validator';

@Module({
  providers: [IsUniqueValidator, IsNotExistValidator, IsExistValidator],
  imports: [PrismaModule],
})
export class CustomValidatorModule {}
