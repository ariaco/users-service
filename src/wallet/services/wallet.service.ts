import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class WalletService {
  constructor(private readonly httpService: HttpService) {}

  async get({ id }) {
    const response = await lastValueFrom(
      this.httpService.get(`${process.env.WALLET_SERVICE_URL}/wallet/${id}`),
    );
    return response.data;
  }

  async update({ id, data }) {
    const response = await lastValueFrom(
      this.httpService.patch(
        `${process.env.WALLET_SERVICE_URL}/wallet/${id}`,
        data,
      ),
    );

    return response.data ? response.data.amount : undefined;
  }
}
