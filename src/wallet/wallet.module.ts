import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { WalletService } from './services/wallet.service';

@Module({
  providers: [WalletService],
  imports: [HttpModule],
  exports: [WalletService],
})
export class WalletModule {}
