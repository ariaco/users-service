FROM node:16-bullseye-slim

WORKDIR /usr/src/application

COPY package*.json ./

RUN npm i

COPY . .

ENTRYPOINT [ "./entrypoint.sh" ]

CMD [ "npm", "run", "start:prod" ]
